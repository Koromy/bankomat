package at.fh.Main;

import java.util.Scanner;

public class main {

    public static Scanner scanner = new Scanner(System.in);
    public static float balance = 0f;
    public static float deposit = 0f;
    public static float payout = 0f;


    public static void main(String[] args){

        boolean running = false;
        System.out.println("Willkommen beim Bankomat!");

        while(!running) {

            boolean isFinished = false;

            System.out.println("Wählen Sie bitte aus:\n0 Einzahlen\n1 Abheben\n2 Kontostand\n3 Beenden");
            int selection = scanner.nextInt();

            while (!isFinished) {

                if (selection == 0) {
                    deposit();
                    isFinished = true;

                } else if (selection == 1) {
                    payout();
                    isFinished = true;

                } else if (selection == 2) {
                    balance();
                    isFinished = true;

                } else if (selection == 3) {
                    cancel();
                    isFinished = true;
                    running = true;

                } else {
                    System.out.println("Falsche Eingabe! Bitte probieren Sie es erneut");
                    isFinished = false;
                }
            }
        }
    }

    public static void deposit(){
        System.out.println("Geben Sie den Betrag ein, den Sie einzahlen wollen");

        deposit = scanner.nextFloat();
        System.out.println("Sie haben " + deposit + "€ eingezahlt");
        balance =+ deposit;
        System.out.println("Ihr neuer Kontostand ist:" + balance + "€");
        deposit = 0;
    }
    public static void payout(){

        boolean enough = false;

        while(!enough){
            System.out.println("Geben Sie den Betrag ein, den Sie abheben wollen");

            payout = scanner.nextFloat();
            if(payout <= balance){
                System.out.println("Sie haben " + payout + "€ abgehoben");
                balance = balance - payout;
                System.out.println("\nIhr neuer Kontostand ist:" + balance + "\n");
                payout = 0;

                enough = true;
            }
            else{
                System.out.println("\nAuszahlungsbetrag zu groß. Bitte versuchen Sie es erneut\n");
                enough = false;
            }
        }
    }
    public static void balance(){

        System.out.println("Hier ist Ihr aktueller Kontostand: " + balance + "€\n");
    }
    public static void cancel(){

        System.out.println("Aufwiedersehen!\n");
    }
    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}

